from os import linesep


def cadastrarNovoSetor():
    codigo = input("Codigo: ")
    nome = input("Nome: ")
    sigla = input("Sigla: ")
    ativo = input("Ativo: ")

    arquivoSetor = open("projeto/projeto-lpa/arquivos/setor.txt", "a",encoding='utf-8')
    arquivoSetor.write(codigo+', '+nome+', '+sigla+', '+ativo+'\n')
    arquivoSetor.close()

def listarSetores():
    arquivoSetor= open("projeto/projeto-lpa/arquivos/setor.txt", "r",encoding='utf-8')
    linhas = arquivoSetor.readlines()
    arquivoSetor.close()

    for index,linha in enumerate(linhas):
        print(f"{index+1} - {linha}",end="")

def alterarSetor():
    arquivoSetor = open("projeto/projeto-lpa/arquivos/setor.txt",'r', encoding='utf-8')
    linhas = arquivoSetor.readlines()
    arquivoSetor.close()

    print("Selecione o setor que deseja editar")
    listarSetores()

    op = int(input("Escolha: "))
    arquivoSetor = open("projeto/projeto-lpa/arquivos/setor.txt",'w+', encoding='utf-8')
    for index,linha in enumerate(linhas):
        if(index+1 == op):
            codigo = input("Codigo: ")
            nome = input("Nome: ")
            sigla = input("Sigla: ")
            ativo = input("Ativo: ")
            arquivoSetor.write(codigo+', '+nome+', '+sigla+', '+ativo+'\n')
            continue

        arquivoSetor.write(linha)
    arquivoSetor.close()
         
def removerSetor():
    arquivoSetor = open("projeto/projeto-lpa/arquivos/setor.txt",'r', encoding='utf-8')
    linhas = arquivoSetor.readlines()
    arquivoSetor.close()

    print("Selecione o setor que desejas remover")
    listarSetores()
    op = int(input("Escolha: "))

    arquivoSetor = open("projeto/projeto-lpa/arquivos/setor.txt",'w+', encoding='utf-8')
    for index, linha in enumerate(linhas):
        if(index+1 == op):
            continue



listarSetores()

